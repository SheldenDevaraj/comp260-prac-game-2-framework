﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class WorkingPuckControl : MonoBehaviour {

	public AudioClip wallCollideClip;
	public AudioClip paddleCollideClip;
	public LayerMask paddleLayer;
	private AudioSource audio;

	// Use this for initialization
	void Start () {
		audio = GetComponent<AudioSource> ();
	}

	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter(Collision collision) {
		Debug.Log("Collision Enter" + collision.gameObject.name);
		if (paddleLayer.Contains (collision.gameObject)) {
			audio.PlayOneShot (paddleCollideClip);
		} else {
			audio.PlayOneShot (wallCollideClip);
		}
	}

	void OnCollisionStay(Collision collision) {
		Debug.Log("Collision Stay" + collision.gameObject.name);
	}

	void OnCollisionExit(Collision collision) {
		Debug.Log("Collision Exit" + collision.gameObject.name);

	}

}

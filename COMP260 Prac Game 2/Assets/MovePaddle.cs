﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]

public class MovePaddle : MonoBehaviour {

	private Rigidbody rigidbody;
	public float speed = 20f;
	public float force = 10f;

	private Vector3 GetMousePosition() {
		// create a ray from the camera 
		// passing through the mouse position
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

		// find out where the ray intersects the XZ plane
		Plane plane = new Plane(Vector3.up, Vector3.zero);
		float distance = 0;
		plane.Raycast(ray, out distance);
		return ray.GetPoint(distance);
	}


	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody>();
		rigidbody.useGravity = false;
	}
	
	// Update is called once per frame
	void Update () {
		Debug.Log("Time = " + Time.time);
	}

	void FixedUpdate () {
		float Horizontal = Input.GetAxis("Horizontal");
		float Vertical = Input.GetAxis("Vertical");
		Vector3 Location = new Vector3 (Horizontal, 0, Vertical);
		Debug.Log("Fixed Time = " + Time.fixedTime);
		//Vector3 pos = GetMousePosition();
		//this.rigidbody.position = pos;
		//Vector3 dir = pos - rigidbody.position;

		//rigidbody.AddForce(dir.normalized * force);
		//Vector3 vel = dir.normalized * speed;
		Vector3 loc = Location.normalized * speed;
		//dir = dir.normalized;
		//rigidbody.velocity = dir * speed;

		// check is this speed is going to overshoot the target
		//float move = speed * Time.fixedDeltaTime;
		//float distToTarget = dir.magnitude;
		//if (move > distToTarget) {
			// scale the velocity down appropriately
			//vel = vel * distToTarget / move;
		//}

		rigidbody.velocity = loc;


	}

	void OnDrawGizmos() {
		// draw the mouse ray
		Gizmos.color = Color.yellow;
		Vector3 pos = GetMousePosition();
		Gizmos.DrawLine(Camera.main.transform.position, pos);
	}

}
//transform.position moves the thing to a destination
//rigidbody.position moves a thing to a destination but also updates the physical effects of the movement
//.velocity moves the thing towards a destonation
//.AddForce moves a thing by applying a force to it (indirect)